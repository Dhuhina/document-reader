//selecting all required elements
const dropArea =document.querySelector(".drag-area"),
dragText = dropArea.querySelector("header"),  
label=dropArea.querySelector("label");
ActualBtn = dropArea.querySelector("actual-btn");

let file;//this is a gobal variable and we will use it inside multiple functios

label.onclick =()=>{
	ActualBtn.click();
}
ActualBtn.addEventListener("change",function(){
	file =this.files[0];
	showFile();//calling function
	dropArea.classList.add("active");
});



//if user Drag File Over DragArea
dropArea.addEventListener("dragover", (event)=>{
	event.preventDefault();//preventing from default behaviour
	//console.log("File is over DragArea");
	dropArea.classList.add("active");
});

//if user leave  dragged File from DragArea
dropArea.addEventListener("dragleave", ()=>{
	//console.log("File is outside from DragArea");
	dropArea.classList.remove("active");
});

//if user Drop File on DragArea
dropArea.addEventListener("drag", (event)=>{
	event.preventDefault();//preventing from default behaviour
	//console.log("File is dropped on DropArea");
	//dropArea.classList.add("active");
	
	file=event.dataTransfer.files[0];
	showFile();//calling function
});

function showFile(){
	let fileType=file.type;
	//console.log(fileType);
	
	let validExtensions=["image/jpeg","image/jpg",image/png"];
	if(validExtensions.includes(fileType)){
		let fileReader=new FileRader();
		fileReader.onload=()=>{
			let fileURL=fileReader.result;
			//console.log(fileURL);
			
			let imgTag = '<img src="${fileURL}" alt="">';
			dropArea.innerHTML = imgTag;
	}
	fileReader.readAsDataURL(file);
	}
    else{
		alert("This is not an Image File!!!");
		dropArea.classList.remove("active");
	}
}